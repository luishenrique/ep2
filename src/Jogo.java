import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Jogo extends JPanel implements KeyListener, ActionListener {
	
	//Posicionar snake;
	private int[] primeiraSnake_Xaxis = new int[750];			//Posiçao da cobra em X
	private int[] primeiraSnake_Yaxis = new int[750];			//Posição da cobra em Y
	
	//Primeiro movimento;
	private int moves = 0;
	
	//Tamanho padrão da snake
	private int tamanhoPadraoCobra = 3;
	
	//Detectar movimento;
	private boolean esquerda = false;
	private boolean direita = false;
	private boolean cima = false;
	private boolean baixo = false;
	
	//Imagens do movimento na cabeça da snake;
	private ImageIcon cabecaDireita;
	private ImageIcon cabecaCima;
	private ImageIcon cabecaBaixo;
	private ImageIcon cabecaEsquerda;
	
	//Imagem do corpo da snake;
	private ImageIcon corpoCobra;
	
	//Velocidade dos frames
	private Timer timer;
	private int delay = 100;
	
	//Posição do alvo 
	private int [] alvoXPos = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	private int [] alvoYPos = {75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625};
	
	private ImageIcon alvoimage;
	
	//Numeros aleatorios para a posição do alvo
	private Random random = new Random();
	
	private int score = 0;
	
	private int xpos = random.nextInt(34);
	private int ypos = random.nextInt(23);
	
	
	
	private ImageIcon imagemTitulo;
	
	public Jogo() {
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		
		timer = new Timer(delay, this);
		timer.start();
	}
	
	public void paint(Graphics g) {
		
		if(moves == 0) {
			primeiraSnake_Xaxis[2] = 50;
			primeiraSnake_Xaxis[1] = 75;
			primeiraSnake_Xaxis[0] = 100;
			
			primeiraSnake_Yaxis[2] = 100;
			primeiraSnake_Yaxis[1] = 100;
			primeiraSnake_Yaxis[0] = 100;
		}
		
		//Desenhar a borda do titulo
		g.setColor(Color.WHITE);
		g.drawRect(24, 10, 851, 55);
		
		//Desenha o titulo
		imagemTitulo = new ImageIcon("tituloJogo.jpg");
		imagemTitulo.paintIcon(this, g, 25, 11);
		
		//Desenhar borda para o Jogo
		g.setColor(Color.WHITE);
		g.drawRect(24, 74, 851, 577);
		
		//Desenhar o fundo para o jogo
		g.setColor(Color.BLACK);
		g.fillRect(25, 75, 850, 575);
		
		//Desenhar pontucação
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Scores: " +score, 780, 30);
		
		//Desenha o tamanho da cobra
		g.setColor(Color.WHITE);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Tamanho: " +tamanhoPadraoCobra, 780, 50);
		
		//Desenhar a snake
		cabecaDireita = new ImageIcon("cabecaDireita.png");
		cabecaDireita.paintIcon(this, g, primeiraSnake_Xaxis[0], primeiraSnake_Yaxis[0]);
		
		//corpo
		for(int i = 0; i < tamanhoPadraoCobra; i++) {
			
			if(i == 0 && direita) {
				cabecaDireita = new ImageIcon("cabecaDireita.png");
				cabecaDireita.paintIcon(this, g, primeiraSnake_Xaxis[i], primeiraSnake_Yaxis[i]);
			}
			if(i == 0 && esquerda) {
				cabecaEsquerda = new ImageIcon("cabecaEsquerda.png");
				cabecaEsquerda.paintIcon(this, g, primeiraSnake_Xaxis[i], primeiraSnake_Yaxis[i]);
			}
			if(i == 0 && baixo) {
				cabecaBaixo = new ImageIcon("cabecaBaixo.png");
				cabecaBaixo.paintIcon(this, g, primeiraSnake_Xaxis[i], primeiraSnake_Yaxis[i]);
			}
			if(i == 0 && cima) {
				cabecaCima = new ImageIcon("cabecaCima.png");
				cabecaCima.paintIcon(this, g, primeiraSnake_Xaxis[i], primeiraSnake_Yaxis[i]);
			}
			if(i != 0) {
				corpoCobra = new ImageIcon("corpoCobra.png");
				corpoCobra.paintIcon(this, g, primeiraSnake_Xaxis[i], primeiraSnake_Yaxis[i]);
			}
			
		}
		
		//Desenha alvo
		alvoimage = new ImageIcon("alvo.png");
		alvoimage.paintIcon(this, g, alvoXPos[xpos], alvoYPos[ypos]);
		
		if((alvoXPos[xpos] == primeiraSnake_Xaxis[0]) && alvoYPos[ypos] == primeiraSnake_Yaxis[0] )
		{
			score++;
			tamanhoPadraoCobra++;
			xpos = random.nextInt(34);
			ypos = random.nextInt(23);
		}
		
		for(int b = 1; b < tamanhoPadraoCobra; b++) {
			if(primeiraSnake_Xaxis[b] == primeiraSnake_Xaxis[0] && primeiraSnake_Yaxis[b] == primeiraSnake_Yaxis[0])
			{
				direita = false;
				cima = false;
				esquerda = false;
				baixo = false;
				
				g.setColor(Color.WHITE);
				g.setFont(new Font("arial", Font.BOLD, 50));
				g.drawString("Game Over", 300, 300);
				
				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Space to RESTART", 350, 340);
			}
		}
		
		g.dispose();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		timer.restart();
		if(direita) {
			for(int r = tamanhoPadraoCobra-1; r >= 0; r--) 
			{
				primeiraSnake_Yaxis[r+1] = primeiraSnake_Yaxis[r];
			}
			for(int r = tamanhoPadraoCobra; r >= 0; r--)
			{
				if(r == 0) 
				{
					primeiraSnake_Xaxis[r] = primeiraSnake_Xaxis[r] + 25;
				}
				else
				{
					primeiraSnake_Xaxis[r] = primeiraSnake_Xaxis[r-1];
				}
				if(primeiraSnake_Xaxis[r] > 850) 
				{
					primeiraSnake_Xaxis[r] = 25;
				}
			}
			
			repaint();
		}
		if(esquerda) {
			for(int r = tamanhoPadraoCobra-1; r >= 0; r--) 
			{
				primeiraSnake_Yaxis[r+1] = primeiraSnake_Yaxis[r];
			}
			for(int r = tamanhoPadraoCobra; r >= 0; r--)
			{
				if(r == 0) 
				{
					primeiraSnake_Xaxis[r] = primeiraSnake_Xaxis[r] - 25;
				}
				else
				{
					primeiraSnake_Xaxis[r] = primeiraSnake_Xaxis[r-1];
				}
				if(primeiraSnake_Xaxis[r] < 25) 
				{
					primeiraSnake_Xaxis[r] = 850;
				}
			}
			
			repaint();
					
		}
		if(baixo) {
			for(int r = tamanhoPadraoCobra-1; r >= 0; r--) 
			{
				primeiraSnake_Xaxis[r+1] = primeiraSnake_Xaxis[r];
			}
			for(int r = tamanhoPadraoCobra; r >= 0; r--)
			{
				if(r == 0) 
				{
					primeiraSnake_Yaxis[r] = primeiraSnake_Yaxis[r] + 25;
				}
				else
				{
					primeiraSnake_Yaxis[r] = primeiraSnake_Yaxis[r-1];
				}
				if(primeiraSnake_Yaxis[r] > 625) 
				{
					primeiraSnake_Yaxis[r] = 75;
				}
			}
			
			repaint();
			
		}
		if(cima) {
			for(int r = tamanhoPadraoCobra-1; r >= 0; r--) 
			{
				primeiraSnake_Xaxis[r+1] = primeiraSnake_Xaxis[r];
			}
			for(int r = tamanhoPadraoCobra; r >= 0; r--)
			{
				if(r == 0) 
				{
					primeiraSnake_Yaxis[r] = primeiraSnake_Yaxis[r] - 25;
				}
				else
				{
					primeiraSnake_Yaxis[r] = primeiraSnake_Yaxis[r-1];
				}
				if(primeiraSnake_Yaxis[r] < 75) 
				{
					primeiraSnake_Yaxis[r] = 625;
				}
			}
			
			repaint();
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
		//Resetar o jogo ao pressionar espaço
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			moves = 0;
			score = 0;
			tamanhoPadraoCobra = 3;
			repaint();
		}
		
		
		
		//Botao seta direita pressionado
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			moves++;
			
			direita = true;
			if(!esquerda) {
				direita = true;
			} 
			else {
				direita = false;
				esquerda = true;
			}
			cima = false;
			baixo = false;
		}
		
		//Botao seta esquerda pressionado
		if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			moves++;
			
			esquerda = true;
			if(!direita) {
				esquerda = true;
			} 
			else {
				esquerda = false;
				direita = true;
			}
			cima = false;
			baixo = false;
		}
		
		//Botao seta cima pressionado
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			moves++;
			
			cima = true;
			if(!baixo) {
				cima = true;
			} 
			else {
				cima = false;
				baixo = true;
			}
			esquerda = false;
			direita = false;
		}
		
		//Botao seta baixo pressionado
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			moves++;
			
			baixo = true;
			if(!cima) {
				baixo = true;
			} 
			else {
				baixo = false;
				cima = true;
			}
			esquerda = false;
			direita = false;
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}