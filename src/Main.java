import java.awt.Color;

import javax.swing.JFrame;

public class Main {
	
	public static void main(String[] args) {
		
		JFrame janela = new JFrame();		//cria Janela;
		Jogo jogo = new Jogo();				//cria tela do jogo dentro da Janela;
		
		
		janela.setBounds(10, 10, 905, 700);										//Ajusta a dimensão da tela;
		janela.setBackground(Color.DARK_GRAY);									//Ajusta cor de fundo da janela;
		janela.setResizable(false);												//Proibe o redimensionamento da janela;
		janela.setVisible(true);												//Visibilidade da janela;
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);					//Encerra a aplicação ao sair;
		
		janela.add(jogo);				//Adiciona a tela do jogo na JFrame Janela;
		
	}
}